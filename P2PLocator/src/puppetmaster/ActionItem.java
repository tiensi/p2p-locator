package puppetmaster;

import puppetmaster.Actions.Action;

//prep an action with data and store it in the list.
public class ActionItem{
	// Action to be performed
	public final Action a;
	// Data provided for that action (GPS coordinates, Phone number, etc)
	public final String num;
	
	public final String msg;
	
	public ActionItem(Action act, String s){
		this.a = act;
		this.num = s;
		this.msg = "";
	}
	public ActionItem(Action act, String num, String data){
		this.a = act;
		this.num = num;
		this.msg = data;
	}
}
