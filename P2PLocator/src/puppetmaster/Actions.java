package puppetmaster;

import java.util.ArrayList;

import android.util.Log;

public class Actions {
	
	private ArrayList<ActionItem> readyactions = new ArrayList<ActionItem>();

	public enum Action{ ADD_RECIPIENT, SEND_INVITE, 
		START_MAP, QUIT,STOP_SENDING, GPS_DATA 
	}
	
	
	/*
	public Actions(){
		try {
			wait();
		} catch (InterruptedException e) {
			Log.e("Actions", e.toString());
			e.printStackTrace();
			// try to prevent null pointers and other strange errors
			// by putting *something* in the array list
			addAction(Action.QUIT,"");
		}
		//There's really nothing to do...
	}
	*/
	// Note: does this need to be synchronized? Is there a race conditoin
	// when two items attempt to add to the readyactions array list?
	public synchronized void addAction(Action newA, String phoneNumber, String msg){
		Log.d("ACTION","Adding action");
		this.readyactions.add(new ActionItem(newA, phoneNumber, msg));
		notify();
	}
	

	
	// This should only ever be called by Puppet Master
	public synchronized ActionItem getAction() {
		if(this.readyactions.isEmpty()){
			try {
				Log.d("ACTIONS","I'm waiting for ready actions");
				Log.d("ACTIONS",Integer.toString(this.readyactions.size()));
				wait();
			} catch (InterruptedException e) {
				Log.e("ACTIONS",e.toString());
				e.printStackTrace();
			}
		}
		return this.readyactions.remove(0);
	}

}
