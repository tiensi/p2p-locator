package puppetmaster;

import java.util.ArrayList;
import java.util.List;

import android.app.PendingIntent;
import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.telephony.SmsManager;
import android.util.Log;


public class SmsSender extends AsyncTask<Void, Void ,Void>{

	private Context mycontext;

	
	// way to externally update contact 
	public ArrayList<String> nums = new ArrayList<String>();
	
	// way to externally end the thread
	public boolean finished = false;
	
	// default wait time between location updates
	private int waitTime = 5000;
	
	public SmsSender(Context context, ArrayList<String> numbers){
		Log.d("SMS_SENDER", "Created with empty sender list.");
		this.mycontext = context;
		this.nums = numbers;
	}
	

	@Override
	protected Void doInBackground(Void... params) {
		Log.d("SMS_SENDER","Going into the background");
		go();
		return null;
	}
	
	
	protected void onPostExecute(Void... params){
		
	}
	
	
	public void go() {
		// Main entry point to SmsSender
		Log.d("SMS_SENDER","SMS Sender has started.");

		// create 10 messages at 1 second intervals
		while(!finished){

			sendMsg();
			try {
				Thread.sleep(waitTime);
			} catch (InterruptedException e) {
				Log.e("SMS_SENDER","ERROR! Intterupted Exception");
				e.printStackTrace();
			}
		}
		sendQuit();
		Log.d("SMS_SENDER","Exiting");
		
	}
	
	
	
	private void sendQuit() {
		Log.d("SMS_SENDER","Going to send Quit message");
		// create and send a sms message right away
	    SmsManager sm = SmsManager.getDefault();
	
	    for(String phoneNum:this.nums){
	        Log.d("SMS_SENDER",phoneNum+":Sending Quit");
	        try{
	        sm.sendTextMessage(phoneNum, 
	        		// use default SMSC (this phone's number)
	        		null , IncomingSMS.P2P_IDENTIFIER+IncomingSMS.P2P_STOP, null, 
	        		// deliveryIntent -I don't care if it is delivered
	        		null );
	        } catch (NullPointerException e) {
	        	// EWW! You don't have GPS
	        	// Carry on...
	        	continue;
	        }
			//Log.d("SMS_SENDER",this.phoneNum+":result:"+sendResult);
	
		}
	}


/*
	private void sendMsg(){
		sendMsg("");
	}
*/
	private void sendMsg() {
		Log.d("SMS_SENDER","Going to send message");
		// create and send a sms message right away
        SmsManager sm = SmsManager.getDefault();
        // sendResult allows us to handle error conditions like not having
        // network access. Currently we aren't doing anything with it though.
        PendingIntent sendResult = null;
        Double[] myloc = getLocation();
        for(String phoneNum:this.nums){
	        Log.d("SMS_SENDER",phoneNum+":Sending message");
	        try{
	        	Log.d("SMS_SENDER",phoneNum+":Sending message:"
	        			+ IncomingSMS.P2P_IDENTIFIER+IncomingSMS.P2P_GPS+
	        		myloc[0].toString()+":"+myloc[1].toString());
	        sm.sendTextMessage(phoneNum, 
	        		// use default SMSC (this phone's number)
	        		null , IncomingSMS.P2P_IDENTIFIER+IncomingSMS.P2P_GPS+
	        		myloc[0].toString()+":"+myloc[1].toString(), sendResult, 
	        		// deliveryIntent -I don't care if it is delivered
	        		null );
	        } catch (NullPointerException e) {
	        	// EWW! You don't have GPS
	        	// Carry on...
	        	continue;
	        }
			//Log.d("SMS_SENDER",this.phoneNum+":result:"+sendResult);
        }
	}
	

	private Double[] getLocation() {
		LocationManager lm = (LocationManager) mycontext.getSystemService(Context.LOCATION_SERVICE);
		List<String> provs = lm.getProviders(true);
		Location loc = null;
		Double[] ret = new Double[2];
		for (int i = 0; i < provs.size(); i++) {
			loc = lm.getLastKnownLocation(provs.get(i));
			if (loc != null) {
				ret[0] = loc.getLatitude();
				ret[1] = loc.getLongitude();
				return ret;
			}
		}
		return null;
	}


}
