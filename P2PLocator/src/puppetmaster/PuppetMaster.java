package puppetmaster;

import java.text.ParseException;
import java.util.ArrayList;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;



public class PuppetMaster extends  AsyncTask<Void, Void, Void>{
	
	
	// This is the object that contains the list of contacts 
	// authorized to receive the location of *this* device
	public ArrayList<String> nums = new ArrayList<String>();
	
	ArrayList<Marker> arrMark = null;

	// This is how other threads will interact with the program
	public Actions actionQueue = null;
	
	// Access to the SmsSender thread
	private SmsSender ss = null;

	// Access to the UI thread
	private Context c = null;

	//private AlertDialog receiveOption;

	public PuppetMaster(ArrayList<String> arrName, ArrayList<String> arrPhone, 
			ArrayList<String> arr3,ArrayList<Marker> almark, GoogleMap mMap, Context c){
		
		this.arrMark = almark;
		this.actionQueue =  com.android.map.MapActivity.getActions();
		this.setC(c);
		
	}
	/*
	// Make a new PuppetMaster to handle incoming textrequest
	PuppetMaster(String phoneNum){
		this.nums.add(phoneNum);
		
	}
	*/
	
	@Override
	protected Void doInBackground(Void... params) {
		Log.d("PUPPET_MASTER","going into the background");
		go();

		return null;
	}
	
	// main entry point to this thread
	// however it is really listening to the actionQeue
	//@Override
	public void go() {
		Log.d("PUPPET_MASTER","I LIVE!!!");
		while(true){
			Log.d("PUPPET_MASTER","Trying to get action");
			ActionItem myaction = actionQueue.getAction();
			Log.d("PUPPET_MASTER","Received action");
			switch(myaction.a){
			case ADD_RECIPIENT:
				// Future improvement: provide a way to ask the user to join
				// instead of forcing them to open the applicatino manually
				Log.d("PUPPET_MASTER","Asking for user approval");
				//popupForUserApproval(myaction);
				// Go my minion! Be FREE!!!
				/*
				if (null == this.ss){
					Log.d("PUPPET_MASTER","ERROR can't create ss");
					this.ss = new SmsSender(myaction.num);
				} else {
					Log.d("PUPPET_MASTER","ERROR no ss");
					this.ss.nums.add(myaction.num);
					ss.run();
				}
				*/
				break;
			case QUIT:
				break;
			// This is the sending part of the ADD_RECIPIENT future improvement
			case SEND_INVITE:
				Log.d("PUPPET_MASTER","up");
				break;
			case START_MAP:
				
				break;
			// This allows for early termination
			case STOP_SENDING:
				if(null != this.ss){
					this.ss.nums.remove(myaction.num);
					if(0 == this.ss.nums.size()){
						this.ss.finished = true;
						this.ss = null;
					}
					
				}
			case GPS_DATA:
				try {
					Log.d("PUPPET_MASTER","About to update");
					updateMarker(myaction.num, myaction.msg);
				} catch (ParseException e) {
					Log.e("PUPPET_MASTER",e.toString());
					e.printStackTrace();
				}
				if(null != this.ss && this.ss.nums.contains(myaction.num)){
					//getOtherLoc();
					//Do something with the GPS data
				}
				break;
			default:
				break;

			}
			
		}
						
	}
		
		
	

	private void updateMarker(String Phone, String location) throws ParseException{
		Log.d("UPDATE_MARKER","At the beginning");
		for (Marker mark: this.arrMark){
			Double d1 = 0.0;
			Double d2 = 0.0;
			Log.d("UPDATE_MARKER","Inside the for loop");
			if (mark.getSnippet().equals(Phone)) {
				try {
					Log.d("UPDATE_MARKER","Snippet equals phone");
					Log.d("PUPPET_MASTER","Locatin information received: "+location);
					String[] loca = location.split(":");
					d1 = Double.parseDouble(loca[0]);
					d2 = Double.parseDouble(loca[1]);
					mark.setPosition(new LatLng(d1, d2)  );
				}catch (IllegalArgumentException e){
					//Error ... skip setting this time.
					Log.e("PUPPET_MASTER","Illegal Argument Exception");
					Log.e("PUPPET_MASTER",e.toString());
					d1 = 0.0;
					d2 = 0.0;
				}
					
			}
		}
	}

	public Context getC() {
		return c;
	}

	public void setC(Context c) {
		this.c = c;
	}
		
		
	
}
		


