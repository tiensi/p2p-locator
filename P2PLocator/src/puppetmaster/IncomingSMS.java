package puppetmaster;

import puppetmaster.Actions.Action;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;
import android.util.Log;


public class IncomingSMS extends BroadcastReceiver{

	// Get the object of SmsManager to find out received sms details
	final SmsManager sms = SmsManager.getDefault();
	// Save off SMS Received string
	private static final String SMS_RECEIVED = "android.provider.Telephony.SMS_RECEIVED";
	// Declare special string that identifies P2P connection
	public final static String P2P_IDENTIFIER = "\u10FE\u01FD\u22FF";
	
	public final static String P2P_REQUEST = "\u11FE";
	public final static String P2P_GPS = "\u12FE";
	public final static String P2P_STOP = "\u13FE";
	
	@Override
	public void onReceive(Context context, Intent intent) {
		Log.d("INCOMING_SMS","I receved something");
		// if the incoming intent is a SMS message
		if(intent.getAction().equals(SMS_RECEIVED)){
			// Retrieves a map of extended data from the intent.
			final Bundle bundle = intent.getExtras();
			
			// temporary ... collect all msgs
			{
				final Object[] pdusObj = (Object[]) bundle.get("pdus");
				SmsMessage currentMessage = SmsMessage.createFromPdu((byte[]) pdusObj[0]);
	            // Grab the contents of the message
	            String message = currentMessage.getDisplayMessageBody();
	            // Determine if it is a message we are interested in
	            Log.d("INCOMING_SMS","Capturing All:"+message);
			}
			
			try {
			     
			    if (bundle != null) {
			         
			        final Object[] pdusObj = (Object[]) bundle.get("pdus");
			         
			        for (int i = 0; i < pdusObj.length; i++) {
			        	//Log.d("SMSRECEIVED", "pdus...whatever that is");
			            SmsMessage currentMessage = SmsMessage.createFromPdu((byte[]) pdusObj[i]);
			            // Grab the contents of the message
			            String message = currentMessage.getDisplayMessageBody();
			            
			            
			            // Determine if it is a message we are interested in
			            if(message.startsWith(P2P_IDENTIFIER)){
			            	
			            	
			            	// This is a message we want!
			            	Log.d("INCOMING_SMS","Received message:"+message);
			            	// Fetch who sent this to us
			            	String senderNum = currentMessage.getDisplayOriginatingAddress();
			            	// now kill the alert so no one else receives it
			            	abortBroadcast();
			            	
			            	Actions myact = com.android.map.MapActivity.getActions();
			            	if (null == myact){
			            		return;
			            	}
			            		
			            	// I couldn't bear to delete this block of code
			            	// This is the block of code that enables offering a "request"
			            	// for someone to join your trusted sharing network. 
			            	/*if(message.startsWith(P2P_IDENTIFIER+P2P_REQUEST)){
			            		// if someone is offering help then add this item to the 
			            		// action queue so that puppet master can prompt the user
			            		Log.d("INCOMING_SMS","Processing p2p request");
			            		Log.d("INCOMING_SMS", "My Context is: " + (null == context));
			            		mypm.actionQueue.addAction(Action.ADD_RECIPIENT, senderNum, context);
			            		Log.d("INCOMING_SMS","p2p request DONE");
			            	} else */
			            	if(message.startsWith(P2P_IDENTIFIER+P2P_STOP)){
			            		Log.d("INCOMING_SMS","Received STOP command");
			            		myact.addAction(Action.STOP_SENDING, senderNum, "");
			            	} else if(message.startsWith(P2P_IDENTIFIER+P2P_GPS)){
			            		Log.d("INCOMING_SMS", "Received GPS data");
			            		myact.addAction(Action.GPS_DATA, senderNum, message.replaceFirst(P2P_IDENTIFIER+P2P_GPS, ""));
			            	}

			            }
			        } // end for loop
			      } // bundle is null
			 
			} catch (Exception e) {
				e.printStackTrace();
			    Log.e("INCOMING_SMS", e.toString());
			     
			}
			
			
		}
		
		
		
		Log.d("INCOMING_SMS", "msg processed");
		
		
	}
	


}
