package com.p2plocator;

import java.util.ArrayList;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.android.map.MapActivity;
public class ContactMenu extends Activity implements OnClickListener{


	


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.contact_menu, menu);
        return true;
    }
    private Button btn = null;
	private Button btn1 = null;
	private Button btn2 = null;
	private ArrayList<String> phoneName = new ArrayList<String>();
	private ArrayList<String> phoneNumber = new ArrayList<String>();
	private ArrayList<String> phoneId = new ArrayList<String>();
	private String listN = "";
	private boolean outside = true;
	private TextView txt = null;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.activity_contact_menu);
	    btn2 = (Button) findViewById(R.id.button1);
	    btn1 = (Button) findViewById(R.id.button2);
	    btn = (Button) findViewById(R.id.button3);
	    txt = (TextView) findViewById(R.id.textView1);
	    btn1.setOnClickListener(this);
	    btn2.setOnClickListener(this);	
	    btn.setOnClickListener(this);
	}
	
	@Override
	public void onClick(View arg0) {
	    if (arg0 == btn) {
	        try {
	            Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
	            startActivityForResult(intent, 1);
	        } catch (Exception e) {
	            e.printStackTrace();
	            Log.e("Error in intent : ", e.toString());
	        }
	    } else if (arg0 == btn1) {
	    	phoneName.clear();
	    	phoneId.clear();
	    	phoneNumber.clear();
	    	txt.setText("No Contacts Selected");
	    } else {
	    	Intent in = new Intent (ContactMenu.this, MapActivity.class);
	   	   	in.putStringArrayListExtra("name_list", phoneName);
	   	   	in.putStringArrayListExtra("num_list", phoneNumber);
	   	   	in.putStringArrayListExtra("id_list", phoneId);
	   	   	startActivity(in);


	    }
	}
	
	
	@Override
	public void onActivityResult(int reqCode, int resultCode, Intent data) {
	    super.onActivityResult(reqCode, resultCode, data);
	
	    try {
	        if (resultCode == Activity.RESULT_OK) {
	            Uri contactData = data.getData();
	            ContentResolver contect_resolver = getContentResolver();
	            Cursor cur = contect_resolver.query(contactData, null, null, null, null);
	            if (cur.moveToFirst()) {
	                String id = cur.getString(cur.getColumnIndexOrThrow(ContactsContract.Contacts._ID));
	                String name = "";
	                String no = "";
	
	                Cursor phoneCur = contect_resolver.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
	                        ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?", new String[] { id }, null);
	
	                if (phoneCur.moveToFirst()) {
	                    name = phoneCur.getString(phoneCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
	                    no = phoneCur.getString(phoneCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
	                    id =  phoneCur.getString(phoneCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.CONTACT_ID));
	                }
	                Log.d("Phone no & name :***: ", name + " : " + no + "id" + id);
	                outside = true;
	                for (int y = 0; y < phoneNumber.size(); y++) {
	                	if (no.compareTo(phoneNumber.get(y)) == 0) {
	                		outside = false;
	                	}
	                }
	                if (outside && name != "") {
		                phoneName.add(name);
		                phoneId.add(id);
		                no = no.replaceAll( "[^\\d]", "" );
		                phoneNumber.add(no);
	                }
	                               	                
	                id = null;
	                name = null;
	                no = null;
	                phoneCur = null;
	            }
	            contect_resolver = null;
	            cur = null;
	            if (phoneName.size() > 0) {	            		
		            listN = phoneName.get(0);
		            for (int x = 1; x < phoneName.size(); x++) {
		                listN += "\n" + phoneName.get(x);
		            }
	            } else {
	            	listN = "No Contacts Selected";
	            }
	            txt.setText(listN);
	        }
	    } catch (IllegalArgumentException e) {
	        e.printStackTrace();
	        Log.e("IllegalArgumentException :: ", e.toString());
	    } catch (Exception e) {
	        e.printStackTrace();
	        Log.e("Error :: ", e.toString());
	    }
	}
	
	
	

	
	


	
	
}
