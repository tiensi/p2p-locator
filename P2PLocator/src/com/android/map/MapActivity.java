package com.android.map;

import java.util.ArrayList;
import java.util.List;

import puppetmaster.Actions;
import puppetmaster.PuppetMaster;
import puppetmaster.SmsSender;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapActivity extends Activity {

	private GoogleMap mMap;
//	private LocationManager lm;

	// keep track of the SmsSender thread
	private SmsSender ss = null;
	// This is to maintain the puppet thread
	private PuppetMaster pm = null;
	private static Actions actions = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		Intent i = getIntent();
		ArrayList<String> numbers = i.getStringArrayListExtra("num_list");
		ArrayList<String> names = i.getStringArrayListExtra("name_list");
		ArrayList<String> id = i.getStringArrayListExtra("id_list");
		
		// start sending out messages to the authroized receivers
		ss = new SmsSender(this,numbers);
		ss.execute();
		
		Log.d("MAP_ACTIVITY","On create is not magical");
		Double[] oxy = {0.0,0.0}; //getLocation();

		Double ox = Double.valueOf(oxy[0]);
		Double oy = Double.valueOf(oxy[1]);
		super.onCreate(savedInstanceState);
		setContentView(com.p2plocator.R.layout.activity_main);
		Double[] longlat = getLocation();

		
		for(String num:numbers){
			Log.d("MAP_ACTIVITY",num);
		}
		for(String num:numbers){
			Log.d("MAP_ACTIVITY",num);
		}
		for(String num:numbers){
			Log.d("MAP_ACTIVITY",num);
		}

		Toast toast1 = Toast.makeText(this.getApplicationContext(),
				longlat[0].toString() + " " + longlat[1].toString(),
				Toast.LENGTH_LONG);
		toast1.show();
		mMap = ((MapFragment) getFragmentManager().findFragmentById(com.p2plocator.R.id.map))
				.getMap();
		mMap.setMyLocationEnabled(true);

		CameraUpdate cu1 = CameraUpdateFactory.newLatLngZoom(new LatLng(
				longlat[0], longlat[1]), 8.0f);
		mMap.animateCamera(cu1);
		CameraUpdate cu2 = CameraUpdateFactory.newLatLngZoom(
				new LatLng(ox, oy), 16.0f);
		mMap.animateCamera(cu2);
		this.pm = new PuppetMaster(names , numbers, id, this.createMarker( names, numbers, id), this.mMap, this);
		this.pm.execute();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(com.p2plocator.R.menu.activity_main, menu);
		return super.onCreateOptionsMenu(menu);
	} 
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case com.p2plocator.R.id.menu_options: {
			
		}
		case com.p2plocator.R.id.app_quit: {
			System.exit(0);
		}
			break;
		}
		return true;
	}
	
	
	

	protected Double[] fetchOtherLocation() {
		// Get location from thread
		Double x = Double.valueOf(39.0);
		Double y = Double.valueOf(-81.0);
		Double[] xy = new Double[2];
		xy[0] = x;
		xy[1] = y;
		return xy;
	}
	
	
	



	private ArrayList<Marker> createMarker(ArrayList<String> arrName, ArrayList<String> arrPhone, ArrayList<String> arr3){
		
		ArrayList<Marker> arrMarker = new ArrayList<Marker>();
		Log.d("PUPPET_MASTER","Size of arrName: "+arrName.size());
		Log.d("PUPPET_MASTER","Size of arrPhone: "+arrPhone.size());
		Log.d("PUPPET_MASTER","Size of arr3: "+arr3.size());
		for (int i = 0; i<arrPhone.size(); i++){
			Double[] initialLoc = getLocation();
			arrMarker.add(mMap.addMarker(new MarkerOptions().position(new LatLng(initialLoc[0],initialLoc[1])).title(arrName.get(i)).snippet(arrPhone.get(i))));	
		}
		return arrMarker;	
	}
	private Double[] getLocation() {
		LocationManager lm = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
		List<String> provs = lm.getProviders(true);
		Location loc = null;
		Double[] ret = new Double[2];
		for (int i = 0; i < provs.size(); i++) {
			loc = lm.getLastKnownLocation(provs.get(i));
			if (loc != null) {
				ret[0] = loc.getLatitude();
				ret[1] = loc.getLongitude();
				return ret;
			}
		}
		return null;
	}


	
	public static Actions getActions(){
		if(null == MapActivity.actions){
			MapActivity.actions = new Actions();
		}
		
		return MapActivity.actions;
	}
		
		

	@Override
	protected void onDestroy() {
    	Log.d("MAP_ACTIVITY", "onDestroy");
		super.onDestroy();
		this.ss.finished = true;
	}
		
}

